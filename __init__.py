"""Elmchey debugger."""

__name__ = 'elmchey'
__all__ = ['Elmchey', 'config', 'GameError']

__author__ = 'Mattias Marka'
__status__ = 'Production'



from .debugger.elmchey import Elmchey, GameError
from .debugger import config
