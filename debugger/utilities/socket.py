from socket import socket


class Socket(socket):

    def recvall(self: object, buffersize: int, *args, **kwargs) -> bytes:
        """Receive large buffersize of data from socket."""
        # Loop until there's no data left.
        # Add data to output every loop.
        data = bytes()
        while len(data) < buffersize:
            packet = self.recv(buffersize - len(data), *args, **kwargs)
            if not packet:
                pass
            data += packet
        return data
