from functools import wraps


def safety(f):
    """Safety decorator for bound methods."""
    @wraps(f)
    def wrapped(self, *args, **kwargs):
        # Try to run method.
        # If an error occures, check with method class __fail method.
        # Raise error based on __fail method.
        try:
            return f(self, *args, **kwargs)
        except Exception as error:
            raise error from self._fail(error)
    return wrapped
