import sys
import socket


HOST, PORT = '::1', 6780
AF, SOCKTYPE, PROTO, CANONNAME, SA = socket.getaddrinfo(HOST, PORT)[0]

BYTEORDER = sys.byteorder
SIGNED = False
