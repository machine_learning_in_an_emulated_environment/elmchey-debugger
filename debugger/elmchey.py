import logging

from .protocol import Protocol
from .config import HOST, PORT, AF, SOCKTYPE
from .utilities.socket import Socket
from .utilities.safety import safety


logger = logging.getLogger('Elmchey')

class ElmcheyError(Exception): pass
class GameError(ElmcheyError): pass


class Elmchey:

    def __init__(self, host = HOST, port = PORT, family = AF, type = SOCKTYPE):
        # Make socket object.
        # Save HOST and PORT for later.
        # Initialize variables.
        self.__socket = Socket(family, type)
        self.__address = (HOST, PORT)
        self.__reset_variables()

    def __reset_variables(self):
        # Reset internal variables to default values.
        # Most variables are set to None before connection is established.
        self.__connected = False
        self.__paused = None
        self.__protocol_version = None
        self.__system = None
        self.__game = None
        self.__frame = None

    def __reset_buttons(self):
        # Set all buttons to unpressed.
        self.button('RIGHT', False)
        self.button('LEFT', False)
        self.button('UP', False)
        self.button('DOWN', False)
        self.button('A', False)
        self.button('B', False)
        self.button('SELECT', False)
        self.button('START', False)

    def __reset_subscriptions(self):
        # Set all subscriptions to False.
        self.subscribe('vblank', False)


    def _fail(self, error):
        # Check if error is dangerous.
        # If it is, disconnect.
        # Return error back to decoratoe for raising.
        if isinstance(error, (ElmcheyError, OSError)):
            self.disconnect()
            return ElmcheyError('Something went wrong. Debugger disconnected.')
        else:
            pass


    def __handshake(self):
        # Get protocol version from socket.
        # Check for startup message in socket.
        # Get system type and game from socket.
        # Set system type and game in debugger.
        self.__protocol_version = Protocol.Recv.protocol_version(self.__socket)
        if Protocol.Recv.message(self.__socket) == Protocol.Config.MESSAGES['STARTUP']:
            logger.debug('Debugger connected.')
            system_type, game = Protocol.Recv.system_startup(self.__socket)
            self.__system = Protocol.Config.SYSTEMS[system_type]
            logger.info(f'System = {self.__system}')
            self.__game = game
            logger.info(f'Game = {self.__game}')


    @safety
    def __listen(self):

        def switch(code):
            RM = Protocol.Config.MESSAGES
            switcher = {
                RM['ERROR']: Protocol.Recv.error,
                RM['MEMORY']: Protocol.Recv.read_memory,
                RM['SCREEN']: Protocol.Recv.screen_buffer
            }
            try:
                return switcher[code]
            except KeyError:
                raise ElmcheyError('Invalid message header.')

        message = Protocol.Recv.message(self.__socket)

        return switch(message)(self.__socket)


    def connect(self):
        """Connect to debuggee."""
        # Check that debugger isn't already connected.
        # If not, then connect to socket and do initial handshake.
        if not self.__connected:
            self.__socket.connect(self.__address)
            self.__handshake()
            self.__connected = True
        else:
            logger.error('Debugger already connected.')


    def disconnect(self):
        """Disconnect from debuggee."""
        # Check that debugger is connected.
        # If it is, then reset variables and disconnect from socket.
        if self.__connected:
            self.__reset_variables()
            self.__socket.close()
            logger.debug('Debugger disconnected.')
        else:
            logger.error('Debugger already disconnected.')


    def run(self):
        """Run emulator."""
        # Send "run" comman to socket.
        # Keep track of debuggee state in debugger.
        Protocol.Send.run(self.__socket)
        self.__paused = False
        logger.debug('Elmchey running.')

    def pause(self):
        """Pause emulator."""
        # Send "pause" command to socket.
        # Keep track of debuggee state in debugger.
        Protocol.Send.pause(self.__socket)
        self.__paused = True
        logger.debug('Elmchey paused.')


    def subscribe(self, event, state):
        """Subscribe to events."""
        # Get valid events.
        # Make event all lower, so that capitalization does not matter.
        # Check if user-given event is valid.
        # If so, then send "subscribe" command to socket.
        events = Protocol.Config.EVENTS
        event = event.lower()
        if event in events:
            Protocol.Send.subscribe(self.__socket, events[event], state)
        else:
            logger.error('Invalid event.')

    def read_memory(self, address, size):
        """Read memory."""
        # Send "read memory" command.
        # Listen for resonse.
        Protocol.Send.read_memory(self.__socket, address, size)
        return self.__listen()

    def write_memory(self, address, value):
        """Write value."""
        # Send "write memory" command.
        # Hope that everything goes well.
        # No need to listen for response.
        Protocol.Send.write_memory(self.__socket, address, value)

    def screen_buffer(self):
        """Get screen buffer."""
        # Send "screen buffer" comamnd to socket.
        # Listen for response.
        Protocol.Send.screen_buffer(self.__socket)
        return self.__listen()

    def button(self, button, state):
        """Press or unpress button."""
        # Get valid buttons.
        # Make button all capitals, so that capitalization does not matter.
        # Check if user-given button is valid.
        # If so, then send "button" command to socket.
        BUTTONS = Protocol.Config.BUTTONS
        button = button.upper()
        if button in BUTTONS:
            Protocol.Send.button(self.__socket, BUTTONS[button], state)
        else:
            logger.error('Invalid button.')


    @safety
    def event(self):
        """Receive events from socket."""
        # Get valid events.
        # Check that response message is correct.
        # If so, receive event type from socket.
        # Keep track of debuggee state in debugger.
        # Return event.
        events = Protocol.Config.EVENTS
        message = Protocol.Recv.message(self.__socket)
        if message == Protocol.Config.MESSAGES['EVENT']:
            self.__paused = True
            event = Protocol.Recv.event(self.__socket)
            return event
        elif message == Protocol.Cofnig.MESSAGES['ERROR']:
            error_code, error_message = Protocol.Recv.error(self.__socket)
            raise ElmcheyError(f'Received error from debuggee: {error_code} {error_message}')
        else:
            raise ElmcheyError('Got unexpected response from debuggee.')


    @property
    def info(self):
        # Get information about debuggee.
        # Return it as a dictionary.
        return {'protocol version': self.__protocol_version,
                'system': self.__system,
                'game': self.__game,
                'paused': self.__paused}


    def frame_stepper(self):
        """Step frames."""
        self.pause()
        self.__reset_subscriptions()
        self.subscribe('vblank', True)
        self.__frame = 1
        self.run()
        while self.event() == Protocol.Config.EVENTS['vblank']:
            yield self.__frame
            self.__reset_buttons()
            self.__frame += 1
            self.run()
