from .config import BYTEORDER, SIGNED


u8, u16 = 1, 2 # Buffer sizes.

class Protocol:

    class Recv:

        class Base:

            @staticmethod
            def integer(sock: object, buffersize: int) -> int:
                # Save bytes from socket.
                # Convert bytes to int.
                message: bytes = sock.recvall(buffersize)
                output: int = int.from_bytes(message, BYTEORDER)
                return output

            @staticmethod
            def string(sock: object, buffersize: int) -> str:
                # Save bytes from socket.
                # Convert bytes to string.
                message: bytes = sock.recvall(buffersize)
                output: str = message.decode()
                return output

        @classmethod
        def message(cls, sock):
            # Receive message head (size = u8) from socket.
            return cls.Base.integer(sock, u8)

        @classmethod
        def string(cls, sock):
            # Receive string length (size = u16) from socket.
            # Receive string with that length.
            # Check for null-terminator.
            length = cls.Base.integer(sock, u16)
            message = cls.Base.string(sock, length)
            assert cls.Base.integer(sock, u8) == 0x0
            return message

        @classmethod
        def protocol_version(cls, sock):
            # Receive protocol version (size = u16) from socket.
            return cls.Base.integer(sock, u16)

        @classmethod
        def system_startup(cls, sock):
            # Recive system type (size = u8).
            # Recive current game.
            system_type = cls.Base.integer(sock, u8)
            game = cls.string(sock)
            return system_type, game

        @classmethod
        def error(cls, sock):
            # Receive error code (size = u8) from socket.
            # Recieve error message from socket.
            code = cls.Base.integer(sock, u8)
            message = cls.string(sock)
            return code, message

        @classmethod
        def event(cls, sock):
            # Receive subscription event type (size = u8) from socket.
            type = cls.Base.integer(sock, u8)
            return type

        @classmethod
        def read_memory(cls, sock):
            # Receive memory dump size (size = u16) from socket.
            # Recive memory dump from socket.
            size = cls.Base.integer(sock, u16)
            area = sock.recvall(u8*size)
            return area

        @classmethod
        def screen_buffer(cls, sock):
            # Receive screen buffer (size = u8*160*144*3) from socket.
            buffer = sock.recvall(u8*160*144*3)
            return buffer

    class Send:

        class Base:

            def integers(sock: object, atoms: list) -> int:
                # Loop for every integer-size pair.
                # Add bytes to output.
                # Send bytes to socket.
                message = bytes()
                for atom in atoms:
                    n, size = atom
                    message += n.to_bytes(size, BYTEORDER, signed = SIGNED)
                return sock.send(message)


        @classmethod
        def message(cls, sock, code):
            # Send message head (size = u8) to socket.
            return cls.Base.integers(sock, [(code, u8)])

        @classmethod
        def run(cls, sock):
            # Send "run" command (code = 0x2) to socket.
            return cls.message(sock, 0x2)

        @classmethod
        def pause(cls, sock):
            # Send "pause" command (code = 0x3) to socket.
            return cls.message(sock, 0x3)

        @classmethod
        def subscribe(cls, sock, event, state):
            # Send "subscribe" command (code = 0x3, size = u8) to socket.
            # Send event type (length = u8) to socket.
            # Send subscription state (length = u8) to socket.
            message = [(0x4, u8), (event, u8), (state, u8)]
            return cls.Base.integers(sock, message)

        @classmethod
        def read_memory(cls, sock, address, size):
            # Send "read memory" command (code = 0x5 size = u8) to socket.
            # Send address (size = u16).
            # Send size (size = u16).
            message = [(0x5, u8), (address, u16), (size, u16)]
            return cls.Base.integers(sock, message)

        @classmethod
        def write_memory(cls, sock, address, value):
            # Send "write memory" command (code = 0x6 size = u8) to socket.
            # Send address (size = u16).
            # Send value (size = u8).
            message = [(0x6, u8), (address, u16), (value, u8)]
            return cls.Base.integers(sock, message)

        @classmethod
        def screen_buffer(cls, sock):
            # Send "screen buffer" comamnd (code = 0x7) to socket.
            return cls.message(sock, 0x7)

        @classmethod
        def button(cls, sock, button, state):
            # Send "button" command (code = 0x6, size = u8) to socket.
            # Send button (size = u8).
            # Send state (size = u8).
            message =[(0x8, u8), (button, u8), (state, u8)]
            return cls.Base.integers(sock, message)

    class Config:

        MESSAGES = {'STARTUP':   0x0,
                    'ERROR':     0x1,
                    'EVENT':     0x2,
                    'MEMORY':    0x3,
                    'SCREEN':    0x4}

        SYSTEMS = {0: 'unknown',
                   1: 'CHIP8',
                   2: 'GameBoy'}

        EVENTS = {'vblank': 0}

        BUTTONS = {'RIGHT':  0x0,
                   'LEFT':   0x1,
                   'UP':     0x2,
                   'DOWN':   0x3,
                   'A':      0x4,
                   'B':      0x5,
                   'SELECT': 0x6,
                   'START':  0x7}

        ERRORS = {0x0: "Invalid command sent.",
                  0x1: "Invalid type sent.",
                  0x2: "Out of bounds read.",
                  0x3: "Invalid button requested."}
